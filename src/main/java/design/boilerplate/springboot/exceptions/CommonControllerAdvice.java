package design.boilerplate.springboot.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

/**
 * Created on February, 2023
 *
 * @author Rohit
 */
@Slf4j
@RestControllerAdvice
public class CommonControllerAdvice {

	@ExceptionHandler(BadCredentialsException.class)
	ResponseEntity<ApiExceptionResponse> handleBadCredentialsException(BadCredentialsException exception) {

		log.info("inside BadCredentialsException");
		final ApiExceptionResponse response = new ApiExceptionResponse(exception.getMessage(), HttpStatus.UNAUTHORIZED, LocalDateTime.now());

		return ResponseEntity.status(response.getStatus()).body(response);
	}


	@ExceptionHandler(TooManyCustomRequestsException.class)
	ResponseEntity<ApiExceptionResponse> handleTooManyCustomRequestsException(TooManyCustomRequestsException exception) {
		log.info("inside TooManyRequestsException");
		final ApiExceptionResponse response = new ApiExceptionResponse(exception.getMessage(), HttpStatus.TOO_MANY_REQUESTS, LocalDateTime.now());

		return ResponseEntity.status(response.getStatus()).body(response);
	}

	@ExceptionHandler(RegistrationException.class)
	ResponseEntity<ApiExceptionResponse> handleRegistrationException(RegistrationException exception) {
		log.info("inside RegistrationController");
		final ApiExceptionResponse response = new ApiExceptionResponse(exception.getMessage(), HttpStatus.BAD_REQUEST, LocalDateTime.now());
		return ResponseEntity.status(response.getStatus()).body(response);
	}

	@ExceptionHandler(Exception.class)
	ResponseEntity<ApiExceptionResponse> handleRegistrationException(Exception exception) {
		log.info("inside Exception");
		final ApiExceptionResponse response = new ApiExceptionResponse(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, LocalDateTime.now());
		return ResponseEntity.status(response.getStatus()).body(response);
	}

}
