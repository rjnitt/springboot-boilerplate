package design.boilerplate.springboot.utils;

import java.util.Locale;

/**
 * Created on February, 2023
 *
 * @author Rohit
 */
public final class ProjectConstants {

	// FIXME : Customize project constants for your application.

	public static final String DEFAULT_ENCODING = "UTF-8";

	public static final String PROJECT_BASE_PACKAGE = "design.boilerplate.springboot";

//	public static final Locale HINDI_LOCALE = new Locale.Builder().setLanguage("hi").setRegion("IN").build();

	private ProjectConstants() {

		throw new UnsupportedOperationException();
	}

}
