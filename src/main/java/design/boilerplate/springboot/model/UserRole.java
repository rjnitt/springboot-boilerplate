package design.boilerplate.springboot.model;

/**
 * Created on February, 2023
 *
 * @author Rohit
 */
public enum UserRole {

	USER, ADMIN

}
