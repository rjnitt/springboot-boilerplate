package design.boilerplate.springboot.security.jwt;

import design.boilerplate.springboot.exceptions.RegistrationException;
import design.boilerplate.springboot.exceptions.TooManyCustomRequestsException;
import design.boilerplate.springboot.security.service.UserDetailsServiceImpl;
import design.boilerplate.springboot.utils.ExceptionMessageAccessor;
import java.util.concurrent.TimeUnit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import static design.boilerplate.springboot.security.utils.SecurityConstants.*;

/**
 * Created on February, 2023
 *
 * @author Rohit
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {

	private final JwtTokenManager jwtTokenManager;

	private final UserDetailsServiceImpl userDetailsService;

	private final ExceptionMessageAccessor exceptionMessageAccessor;

	LoadingCache<String, Integer> counter = CacheBuilder.newBuilder()
		.expireAfterWrite(1, TimeUnit.MINUTES)
		.build(new CacheLoader<String, Integer>() {
						public Integer load(String key) {
							return 0;
						}
					});

	private static final String USERNAME_ALREADY_EXISTS = "username_already_exists";

	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
		System.out.println("i am in do filter");
		String clientIp = req.getRemoteAddr();
		int requests = counter.getUnchecked(clientIp) + 1;
		if (requests > 10) {
			final String tooMany = exceptionMessageAccessor.getMessage(null, "too_many_exists");
			 throw new TooManyCustomRequestsException(tooMany);
//			final String existsUsername = exceptionMessageAccessor.getMessage(null, USERNAME_ALREADY_EXISTS);
//			throw new RegistrationException(existsUsername);
		}
		counter.put(clientIp, requests);
		
		final String requestURI = req.getRequestURI();
		if (requestURI.contains(LOGIN_REQUEST_URI) || requestURI.contains(REGISTRATION_REQUEST_URI)) {
			chain.doFilter(req, res);
			return;
		}

		final String header = req.getHeader(HEADER_STRING);
		String username = null;
		String authToken = null;
		if (Objects.nonNull(header) && header.startsWith(TOKEN_PREFIX)) {

			authToken = header.replace(TOKEN_PREFIX, StringUtils.EMPTY);
			try {
				username = jwtTokenManager.getUsernameFromToken(authToken);
			}
			catch (Exception e) {
				log.error("Authentication Exception : {}", e.getMessage());
			}
		}

		final SecurityContext securityContext = SecurityContextHolder.getContext();

		if (Objects.nonNull(username) && Objects.isNull(securityContext.getAuthentication())) {

			final UserDetails userDetails = userDetailsService.loadUserByUsername(username);

			if (jwtTokenManager.validateToken(authToken, userDetails.getUsername())) {

				final UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));
				log.info("Authentication successful. Logged in username : {} ", username);
				securityContext.setAuthentication(authentication);
			}
		}

		chain.doFilter(req, res);
	}
}
