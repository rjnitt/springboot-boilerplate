
# springboot-boilerplate

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
=======
# Spring Boot Boilerplate
 *Spring Boot Boilerplate* is a **starter kit**. This project is a very simple and useful.
 
## Tech-stack 
- Spring Boot (v2.7.4)
- Spring Data JPA
- Spring Validation
- Spring Security + JWT Token
- Rate Limitation
- Mysql
- Mapstruct
- Lombok
- Swagger


## Run the Application

Navigate to the root of the project. For building the project using command line, run below command :

``` mvn clean install```

Run service in command line. Navigate to *target* directory. 

``` java -jar spring-boot-boilerplate.jar ```

## context path 
context-path=/test
base-url: localhost:8080/test
test-url: {{base-url}}/hello

## Swagger url:
http://localhost:8080/test/swagger-ui/index.html#/

>>>>>>> 811c956 (first)
